import React, { Component } from "react";
import logo from "./logo.png";
import Avatar from "react-avatar";
import { Picker } from "emoji-mart";
import TextareaAutosize from "react-autosize-textarea";
import ImageUploader from "react-images-upload";
import ChatSession from "./ChatSession";
import { Selector } from "react-giphy-selector";
import {
  handleInput,
  toggleEmojiPicker,
  addEmoji,
  connectToChatkit,
  handleKeyPress,
  sendMessage,
  onDrop,
  toggleImagePicker,
  toggleChatWidget,
  toggleGiphyPicker,
  sendGif
} from "./methods";

import "./swipe/html/dist/css/lib/bootstrap.min.css";
import "./swipe/html/dist/css/swipe.min.css";
import "emoji-mart/css/emoji-mart.css";
import "./App.css";

class App extends Component {
  constructor() {
    super();
    this.state = {
      userId: "George",
      showEmojiPicker: false,
      newMessage: "",
      rooms: [],
      currentUser: {},
      currentRoom: null,
      messages: [],
      showImagePicker: false,
      showChatWidget: true,
      showGiphyPicker: false,
      pictures: []
    };

    this.handleInput = handleInput.bind(this);
    this.toggleEmojiPicker = toggleEmojiPicker.bind(this);
    this.addEmoji = addEmoji.bind(this);
    this.connectToChatkit = connectToChatkit.bind(this);
    this.handleKeyPress = handleKeyPress.bind(this);
    this.sendMessage = sendMessage.bind(this);
    this.onDrop = onDrop.bind(this);
    this.toggleImagePicker = toggleImagePicker.bind(this);
    this.toggleChatWidget = toggleChatWidget.bind(this);
    this.toggleGiphyPicker = toggleGiphyPicker.bind(this);
    this.sendGif = sendGif.bind(this);
  }

  componentDidMount() {
    this.connectToChatkit();
  }

  render() {
    const {
      showEmojiPicker,
      newMessage,
      messages,
      currentUser,
      showImagePicker,
      showGiphyPicker,
      currentRoom,
      showChatWidget
    } = this.state;

    return (
      <div className="App">
        <section className="stream" />
        {showChatWidget ? (
          <section className="chat">
            <div className="item">
              <div className="container">
                <header className="top">
                  <div className="headline">
                    {currentRoom ? (
                      <img src={logo} alt="" />
                    ) : (
                      <Avatar
                        color={"green"}
                        name={currentRoom ? currentRoom.name : "Chat"}
                        size={"42px"}
                        round={true}
                      />
                    )}
                    <div className="content">
                      <h5>{currentRoom ? currentRoom.name : "Chat"}</h5>
                    </div>
                  </div>

                  <a
                    href="https://pusher.com/chatkit"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="btn"
                    data-utility="open"
                  >
                    <i className="eva-hover">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        className="eva eva-info eva-animation eva-icon-hover-pulse"
                      >
                        <g data-name="Layer 2">
                          <g data-name="info">
                            <rect
                              width="24"
                              height="24"
                              transform="rotate(180 12 12)"
                              opacity="0"
                            />
                            <path d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm1 14a1 1 0 0 1-2 0v-5a1 1 0 0 1 2 0zm-1-7a1 1 0 1 1 1-1 1 1 0 0 1-1 1z" />
                          </g>
                        </g>
                      </svg>
                    </i>
                  </a>
                </header>
              </div>
              <section className="middle">
                <div className="container">
                  <ul className="chat-messages">
                    {currentRoom ? (
                      <ChatSession
                        messages={messages}
                        currentUser={currentUser}
                      />
                    ) : null}
                    <div
                      ref={node => {
                        this.messagesEnd = node;
                      }}
                    />
                  </ul>
                </div>
              </section>
              <div className="container">
                <footer className="bottom">
                  {showEmojiPicker ? (
                    <Picker set="emojione" onSelect={this.addEmoji} />
                  ) : null}

                  {showGiphyPicker ? (
                    <Selector
                      className="giphy-picker"
                      apiKey={"aOQEA4zUpzkhxy3hdr0RGMJuQ8rV0wIv"}
                      showGiphyMark={false}
                      queryInputPlaceholder={"Search GIFs"}
                      resultColumns={2}
                      onGifSelected={this.sendGif}
                    />
                  ) : null}
                  <form>
                    <TextareaAutosize
                      value={newMessage}
                      onChange={this.handleInput}
                      onKeyPress={this.handleKeyPress}
                      name="newMessage"
                      className="form-control"
                      placeholder="Type message..."
                      rows={1}
                    />
                    <div className="icons">
                      <button
                        className="btn gif-picker"
                        type="button"
                        onClick={this.toggleGiphyPicker}
                      >GIF</button>
                      <button
                        onClick={this.toggleImagePicker}
                        type="button"
                        className="btn image-picker"
                      >
                        <svg id="icon-image" viewBox="0 0 32 32">
                          <path d="M29.996 4c0.001 0.001 0.003 0.002 0.004 0.004v23.993c-0.001 0.001-0.002 0.003-0.004 0.004h-27.993c-0.001-0.001-0.003-0.002-0.004-0.004v-23.993c0.001-0.001 0.002-0.003 0.004-0.004h27.993zM30 2h-28c-1.1 0-2 0.9-2 2v24c0 1.1 0.9 2 2 2h28c1.1 0 2-0.9 2-2v-24c0-1.1-0.9-2-2-2v0z" />
                          <path d="M26 9c0 1.657-1.343 3-3 3s-3-1.343-3-3 1.343-3 3-3 3 1.343 3 3z" />
                          <path d="M28 26h-24v-4l7-12 8 10h2l7-6z" />
                        </svg>
                      </button>
                      <button
                        type="button"
                        onClick={this.toggleEmojiPicker}
                        className="btn emoji-picker"
                      >
                        <svg id="icon-smile" viewBox="0 0 32 32">
                          <path d="M16 32c8.837 0 16-7.163 16-16s-7.163-16-16-16-16 7.163-16 16 7.163 16 16 16zM16 3c7.18 0 13 5.82 13 13s-5.82 13-13 13-13-5.82-13-13 5.82-13 13-13zM8 10c0-1.105 0.895-2 2-2s2 0.895 2 2c0 1.105-0.895 2-2 2s-2-0.895-2-2zM20 10c0-1.105 0.895-2 2-2s2 0.895 2 2c0 1.105-0.895 2-2 2s-2-0.895-2-2zM22.003 19.602l2.573 1.544c-1.749 2.908-4.935 4.855-8.576 4.855s-6.827-1.946-8.576-4.855l2.573-1.544c1.224 2.036 3.454 3.398 6.003 3.398s4.779-1.362 6.003-3.398z" />
                        </svg>
                      </button>
                    </div>
                  </form>
                  <section>
                    {showImagePicker ? (
                      <ImageUploader
                        withIcon={false}
                        buttonText="Choose images"
                        onChange={this.onDrop}
                        imgExtension={[".jpg", ".jpeg", ".png", ".gif"]}
                        maxFileSize={5242880}
                        withPreview={true}
                      />
                    ) : null}
                  </section>
                </footer>
              </div>
            </div>
          </section>
        ) : null}
        <button className="toggle-chat" onClick={this.toggleChatWidget}>
          {!showChatWidget ? (
            <svg id="icon-send" viewBox="0 0 20 20">
              <path d="M0 0l20 10-20 10v-20zM0 8v4l10-2-10-2z" />
            </svg>
          ) : (
            <svg id="icon-cross" viewBox="0 0 32 32">
              <path d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z" />
            </svg>
          )}
        </button>
      </div>
    );
  }
}

export default App;
